# ShowOptionDialog
## $('#btn').showOptionDialogClick(msg, title);
####Sample:
```
$('#btn-teste').showOptionDialogClick('Deseja confirmar alguma coisa?', 'Confirmação!')
    .done(function () {
        // they pressed OK
        alert('Ok');
    })
    .fail(function () {
        // the pressed Cancel
        alert('Cancel');
    });
```
##$('#btn').showOptionDialog(msg, title);
####Sample:
```
$('#btn-teste').click(function(){
  $(this).showOptionDialog('Deseja confirmar alguma coisa?', 'Confirmação!')
    .done(function () {
        // they pressed OK
        alert('Ok');
    })
    .fail(function () {
        // the pressed Cancel
        alert('Cancel');
    });
});
```

## $('#btn').showMessageDialogClick(msg, title);
####Sample:
```
$('#btn-teste').showMessageDialogClick('Deseja confirmar alguma coisa?', 'Confirmação!');
```
## $('#btn').showMessageDialog(msg, title);
####Sample:
```
$('#btn-teste').click(function(){
    $(this).showMessageDialog('Deseja confirmar alguma coisa?', 'Confirmação!')
});
```
