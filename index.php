<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="css/jquery-ui.css">
        <script src="js/Jquery-v1.10.2.js"></script>
        <script src="js/Jquery-UI-build.js"></script>
    </head>
    <body>
        <p>Olá Mundo!</p>
        <button type="button" id="btn-teste">Show</button>

        <script>
            $('#btn-teste').showOptionDialogClick('Deseja confirmar alguma coisa?', 'Confirmação!')
                    .done(function () {
                        // they pressed OK
                        alert('Ok');
                    })
                    .fail(function () {
                        // the pressed Cancel
                        alert('Cancel');
                    });


        </script>
    </body>
</html>
